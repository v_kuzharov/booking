// Imports Base
import '../init'


// Imports views
import './shared'
import '../../views/welcome'
import '../../views/profile'
import '../../views/profile_builder'
import '../../views/users'
import '../../views/search'
