// Without Sprockets, Rails would not understand
// Unobtrusive JavaScript (that includes setting method: :delete on link_to)
import Rails from "rails-ujs";
Rails.start();

// Getting rid from jquery in future. And add Vue.js

import 'bootstrap/js/dist/alert'
// import 'bootstrap/js/dist/button'
// import 'bootstrap/js/dist/carousel'
import 'bootstrap/js/dist/collapse'
// import 'bootstrap/js/dist/dropdown'
// import 'bootstrap/js/dist/index'
import 'bootstrap/js/dist/modal'
// import 'bootstrap/js/dist/popover'
// import 'bootstrap/js/dist/scrollspy'
// import 'bootstrap/js/dist/tab'
// import 'bootstrap/js/dist/toast'
// import 'bootstrap/js/dist/tooltip'
// import 'bootstrap/js/dist/util'

import '../init/deferred.sass'
import '../../views/shared/modals/signup/signup'
import '../../views/shared/modals/login/login'
import '../init/vue-base'


setTimeout(() => {
    $("#flash-messages").fadeTo(900, 0).slideUp(500, function() {
        return $(this).remove();
    });
}, 2000);
