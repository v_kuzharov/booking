import axios from 'axios'

const http = axios.create({
    baseURL: 'https://' +(process.env.DOMAIN_NAME_WEB || 'bookbjj-staging.goodsearchcorp.com')
});

http.defaults.headers.common['X-CSRF-Token'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

export default http
