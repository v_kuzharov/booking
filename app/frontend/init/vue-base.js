// Initialize core Vue.js
import Vue from 'vue/dist/vue.esm'
import Vuelidate from 'vuelidate'
import VueEventCalendar from 'vue-event-calendar'

import SignUpForm from "../../views/shared/modals/signup/vuejs/SignUpForm";
import LoginForm from "../../views/shared/modals/login/vuejs/LoginForm";
import EventCalendar from "../../views/shared/event-calendar/EventCalendar";
import MerchantImagesForm from "../../views/profile_builder/vuejs/MerchantImagesForm";

Vue.use(Vuelidate);
Vue.use(VueEventCalendar, {locale: 'en'});

Vue.component('validation-errors', {
    data(){
        return {

        }
    },
    props: ['errors'],
    template: `<div v-if="validationErrors">
                  <ul class="alert alert-danger">
                      <li v-for="(value, key, index) in validationErrors">{{ value }}</li>
                  </ul>
              </div>`,
    computed: {
        validationErrors(){
            let errors;

            if (typeof this.errors === 'string') {
                errors = [ this.errors ];
            } else {
                errors = Object.entries(this.errors)
                                .map(([key, value]) => `${key.charAt(0).toUpperCase() + key.slice(1)} ${value}`)
                                .flat();
            }

            return errors;
        }
    }
});

document.addEventListener('DOMContentLoaded', () => {
    new Vue({
        el: '#book-bjj',
        components: {
            'sign-up-form': SignUpForm,
            'login-form': LoginForm,
            'merchant-images-form': MerchantImagesForm,
            'event-calendar': EventCalendar
        }
    });
});
