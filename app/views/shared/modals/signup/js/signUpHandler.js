class SignUpHandler {
    constructor() {
        this.signUpLink = document.querySelector('.sign-in-link');
        this.loginModal = $('#login-modal');
        this.signUpModal = $('#sign-up-modal');

        this.setupBindings();
    }

    setupBindings() {
        this.toggleSignUp(this.loginModal, this.signUpModal);
    }

    toggleSignUp(login, signUp) {
        this.signUpLink.addEventListener('click', function (e) {
            e.preventDefault();
            signUp.modal('hide');
            login.modal('show');
        })
    }
}

export default SignUpHandler;
