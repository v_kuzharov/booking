import './signup.sass'
import SignUpHandler from './js/signUpHandler'

window.addEventListener('DOMContentLoaded', () => {
    new SignUpHandler()
});

