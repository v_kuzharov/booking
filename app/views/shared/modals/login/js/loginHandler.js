class LoginHandler {
    constructor() {
        this.signUpLink = document.querySelector('.sign-up-link');
        this.loginModal = $('#login-modal');
        this.signUpModal = $('#sign-up-modal');

        this.setupBindings();
    }

    setupBindings() {
        this.toggleLogin(this.loginModal, this.signUpModal);
    }

    toggleLogin(login, signUp) {
        this.signUpLink.addEventListener('click', function (e) {
            e.preventDefault();
            login.modal('hide');
            signUp.modal('show');
        })
    }
}

export default LoginHandler;
