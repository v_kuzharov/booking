import './header.sass'
import './img/bjj-logo.png'
import './img/mobile-logo.png'
import HeaderHandler from './js/headerHandler'


window.addEventListener('DOMContentLoaded', () => {
    if (window.location.pathname === '/') new HeaderHandler()
});
