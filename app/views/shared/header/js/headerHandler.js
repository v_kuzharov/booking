'use strict';

class HeaderHandler {
    constructor() {
        this.header = document.querySelector('.fixed-top');
        this.collapseBtn = document.querySelector('.collapsable-button');

        this.setupBindings();
    }

    setupBindings() {
        this.initStickyHeader(this.header);
        this.animatedIconToggler();
    }

    initStickyHeader(header) {
        window.addEventListener('scroll', function () {
            if (window.scrollY > 150) {
                header.classList.add('sticky')
            } else {
                header.classList.remove('sticky')
            }
        });

        if (window.scrollY > 150) header.classList.add('sticky')
    }

    animatedIconToggler() {
        this.collapseBtn.addEventListener('click', function () {
            $('.animated-icon').toggleClass('open');
        })
    }
}

export default HeaderHandler;
