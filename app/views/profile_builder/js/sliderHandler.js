'use strict';

class sliderHandler {
    constructor() {
        this.rangeSlider = document.getElementById("rs-range-line");
        this.rangeBullet = document.getElementById("rs-bullet");
        this.rangeFill = document.getElementById("range-fill");

        this.showSliderValue = this.showSliderValue.bind(this);

        this.setupBindings();
    }

    setupBindings() {
        this.showSliderValue();
        this.initSliderEvent();
    }

    initSliderEvent() {
        this.rangeSlider.addEventListener("input", this.showSliderValue, false);
    }

    showSliderValue() {
        this.rangeBullet.innerHTML = this.rangeSlider.value;

        let bulletPosition = (this.rangeSlider.value /this.rangeSlider.max);
        this.rangeBullet.style.left = (bulletPosition * 578) + "px";
        this.rangeFill.style.width = (bulletPosition * 578) + "px";
    }
}

export default sliderHandler;
