import './styles/profile-builder.sass'
import './styles/start.sass'
import './styles/gym_location.sass'
import './styles/listing_name.sass'
import './styles/pricing.sass'
import './styles/specialization.sass'
import './styles/images.sass'
import './styles/services_offered.sass'
import './styles/rules.sass'
import './styles/cancellation.sass'
import './styles/payment.sass'



import './img/calendar.svg'
import './img/credit-card.svg'
import './img/money.svg'
import './img/search.svg'
import './img/Bitmap.png'

import sliderHandler from './js/sliderHandler'


window.addEventListener('DOMContentLoaded', () => {
    const rangeBlock = document.querySelector(".range-block");

    if(rangeBlock) new sliderHandler();
});
