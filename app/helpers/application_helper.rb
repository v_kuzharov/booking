module ApplicationHelper
  def flash_messages!
    output = flash.map do |type, massage|
      key = case type
            when 'notice'
              'success'
            when 'alert'
              'danger'
            else
              type
            end
      content_tag(:div, id: 'flash-messages' ) do
        content_tag(:div, class: "alert alert-#{key} alert-dismissible fade show", role: 'alert') do
          concat(massage)
          concat(content_tag(:button, class: 'close', aria: { label: 'Close' }, data: { dismiss: 'alert' }, type: 'button') do
            content_tag(:span, raw('&#215;'), aria: { hidden: true })
          end)
        end
      end
    end.join
    raw(output)
  end

  def landing_page?
    current_page?(controller: 'welcome')
  end

  # need better solution!
  # styles in frontend/init/custom/global.sass
  def body_padding(page)
    'body-padding'.tap do |klass|
      case page
      when 'welcome'
        klass << '-welcome'
      when 'search'
        klass << '-search'
      when 'profile'
        klass << '-profile' if params[:action] == 'show'
      else
        ''
      end
    end
  end

  def landing_header
    'sticky' unless landing_page?
  end

  def nav_link_class(path)
    'nav-link'.tap { |class_name| class_name << (current_page?(path) ? ' active text-purple' : ' text-dark') }
  end

  def to_snake_case(str)
    return if str.blank?

    str.parameterize.underscore
  end

  def sign_up_or_navigate_to(name, path, opt = {})
    url = user_signed_in? ? path : '#'
    opt[:class] = 'nav-link nobreak'

    if opt[:method].eql?(:delete) && !user_signed_in?
      opt[:class] << ' btn big-yellow-btn'
      name = t('shared.header.header.nav.signup')
    end

    unless user_signed_in?
      opt[:data] = { target: '#sign-up-modal', toggle: 'modal' }
      opt.except!(:method)
    end

    link_to name, url, opt
  end
end
