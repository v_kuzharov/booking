# frozen_string_literal: true

class Merchant < User

  scope :with_data, -> { joins(:user_data).select('users.*, user_data.data') }
  scope :by_id, ->(id) { with_data.find(id) }
end
