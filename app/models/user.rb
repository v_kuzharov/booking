# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :trackable

  # Uploaders
  mount_uploader :avatar, AvatarUploader

  # UTC -4  EDT   Eastern Daylight Time   New York
  # UTC -5  CDT   Central Daylight Time   Chicago
  # UTC -6  MDT   Mountain Daylight Time  Salt Lake City
  # UTC -7  MST   Mountain Standard Time  Phoenix
  # UTC -7  PDT   Pacific Daylight Time   Los Angeles
  # UTC -8  AKDT  Alaska Daylight Time    Anchorage
  # UTC -10 HST   Hawaii Standard Time    Honolulu

  enum time_zone: { EDT: 0, CDT: 1, MDT: 2, MST: 3, PDT: 4, AKDT: 5, HST: 6 }

  ZONE_MAPPING = {
      time_zones[:EDT] => 'New York (Eastern Time)',
      time_zones[:CDT] => 'Chicago (Central Time)',
  }.freeze

  EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/.freeze

  # Better idea is to put this in Admin
  RULES = ['No Food', 'No Kids', 'No Recording'].freeze

  # Better idea is to put this in Admin
  CANCELLATION = %w[Relaxed Average Strict].freeze

  has_one :user_data, dependent: :destroy
  has_many :user_images, dependent: :destroy

  def admin?
    is_a?(Admin)
  end

  def customer?
    is_a?(Customer)
  end

  def merchant?
    is_a?(Merchant)
  end
end
