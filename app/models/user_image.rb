class UserImage < ApplicationRecord

  # Uploaders
  mount_uploader :image, MerchantImagesUploader

  belongs_to :user
end
