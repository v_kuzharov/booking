# frozen_string_literal: true

class ProfileBuilderPolicy < ApplicationPolicy

  def show?
    user.admin? || (user.merchant? && !user.wizard_complete?)
  end
end
