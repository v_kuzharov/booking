class ApplicationDecorator < Draper::Decorator
  # Define methods for all decorated objects.
  # Helpers are accessed through `helpers` (aka `h`).

  delegate_all

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def created
    object.created_at.strftime('%d/%m/%Y')
  end
end
