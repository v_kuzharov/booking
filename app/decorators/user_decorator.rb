class UserDecorator < ApplicationDecorator

  def full_name
    [first_name, last_name].join(' ')
  end
end
