class MerchantDecorator < UserDecorator
  def hourly_rate
    h.number_to_currency(object.data['pricing']['hourly_rate'], precision: 0)
  end
end
