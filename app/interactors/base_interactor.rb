# frozen_string_literal: true

class BaseInteractor
  include ActiveModel::Validations
  extend ActiveModel::Callbacks

  define_model_callbacks :initialize, only: :after

  after_initialize :valid?

  # The call method of a Interactor should always return itself
  # Raises WebValidationsFailure exception
  # if validates or inners methods will fail
  def self.call!(*args)
    new(*args).tap(&:perform!)
  end

  # Returns @errors object with errors messages
  # if validates or inners methods will fail
  def self.call(*args)
    new(*args).tap(&:perform)
  end

  def initialize(args)
    run_callbacks :initialize do
      args.each { |key, val| instance_variable_set("@#{key}", val) unless val.nil? }
    end
  end

  def success?
    errors.none?
  end

  def perform
    return errors.messages unless success?

    perform!
  rescue StandardError => e
    errors.add(:base, e.message)
  end

  def perform!
    raise InteractorFailure.new('', errors_messages: errors.messages) unless success?

    call
  end

  # It is expected that the "call" instance method is overwritten for
  # each service class.
  #
  # Returns nothing.
  def call; end
end

class InteractorFailure < StandardError
  attr_reader :status, :errors_messages

  def initialize(message, errors_messages: [], status: :bad_request)
    @errors_messages = errors_messages
    @status = status
    super(message)
  end

  def custom_errors
    { status: status, errors_msg: errors_messages.presence || message }
  end
end
