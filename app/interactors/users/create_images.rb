class Users::CreateImages < BaseInteractor

  # Define interactor attributes
  attr_reader :user_images

  # Validate incoming parameters

  validates :images,
            presence: true

  validate :verify_images_upload_size

  validate :verify_images_current_size



  def call
    images.each { |item| user_imgs.create!(image: item) }
    user_imgs.reload
  end

  private

  attr_reader :images, :user

  def user_imgs
    @user_images ||= user.user_images
  end

  def verify_images_upload_size
    errors.add(:base, '9 images maximum!') if images.length > 9
  end

  def verify_images_current_size
    return true if user_imgs.blank?

    errors.add(:base, 'You already uploaded 9 images! Remove something.') if user_imgs.size > 9
  end
end
