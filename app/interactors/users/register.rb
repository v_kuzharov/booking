class Users::Register < BaseInteractor
  include Rails.application.routes.url_helpers

  # Define interactor attributes
  attr_reader :user, :after_signup_path

  # Validate incoming parameters
  validates :first_name,
            length: 3..20

  validates :last_name,
            length: 3..20

  validates :email,
            uniqueness: { case_sensitive: false, model: User },
            format: { with: User::EMAIL_REGEXP, message: "it's not an email!" }

  validates :password,
            length: 6..20

  validates :time_zone,
            presence: true

  validates :phone,
            presence: true

  validates :terms,
            acceptance: true

  validates :entity,
            presence: true



  def call
    ActiveRecord::Base.transaction do
      @user = user_entity.create!(user_attributes)
      @user.create_user_data(data: user_data)
      detect_after_sign_up_path
    end
  end

  private

  attr_reader :email, :password, :first_name, :last_name, :time_zone, :phone, :terms, :entity

  def user_entity
    entity.capitalize.constantize
  end

  def detect_after_sign_up_path
    @after_signup_path = @user.customer? ? profile_path(@user.id) : profile_builder_path(:gym_location)
  end

  def user_attributes
    {
        first_name: first_name,
        last_name: last_name,
        email: email,
        password: password,
        password_confirmation: password,
        time_zone: time_zone.to_i
    }
  end

  def user_data
    {
        phone: phone
    }
  end
end
