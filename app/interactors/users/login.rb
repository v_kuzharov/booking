class Users::Login < BaseInteractor
  include Rails.application.routes.url_helpers

  # Define interactor attributes
  attr_reader :user, :after_signin_path

  # Validate incoming parameters

  validates :email,
            presence: true,
            format: { with: User::EMAIL_REGEXP, message: "it's not an email!" }

  validates :password,
            length: 6..20



  def call
    detect_user!
    validate_password!
    detect_after_sign_in_path
  end

  private

  attr_reader :email, :password

  def detect_user!
    @user = User.find_by(email: email)
    raise InteractorFailure.new("User with email: #{email}, not found!", status: :unauthorized) if @user.nil?
  end

  def validate_password!
    raise InteractorFailure.new('Invalid Password!', status: :unauthorized) unless @user.valid_password?(password)
  end

  def detect_after_sign_in_path
    @after_signin_path = begin
      if @user.customer?
        profile_users_path
      elsif @user.merchant?
        bookings_profile_index_path
      else
        profile_builder_path(:gym_location)
      end
    end
  end
end
