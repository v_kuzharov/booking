module Users
  module Forms
    class Rules
      include Users::Form

      attr_accessor :rules_apply, :no_food, :no_kids, :no_recording, :rules_travel, :rules_additional

      validates :rules_apply,
                presence: true

      validates :rules_travel,
                presence: true

      validates :rules_additional,
                presence: true



      def submit(params)
        super
        save_values!(key: object_key, val: { rules_apply: rules_apply, rules_travel: rules_travel, rules_additional: rules_additional })
      end
    end
  end
end
