module Users
  module Forms
    class ListingName
      include Users::Form

      attr_accessor :listing_name, :listing_description

      validates :listing_name,
                presence: true

      validates :listing_description,
                presence: true


      def submit(params)
        super
        save_values!(key: object_key, val: { name: listing_name, description: listing_description})
      end
    end
  end
end
