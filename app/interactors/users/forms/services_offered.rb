module Users
  module Forms
    class ServicesOffered
      include Users::Form

      attr_accessor :services_private_session, :services_roll_review, :services_skype_call, :services_seminar

      validates :services_private_session,
                presence: true

      validates :services_private_session,
                presence: true

      validates :services_private_session,
                presence: true



      def submit(params)
        super
        save_values!(key: object_key, val: { private_session: services_private_session, roll_review: services_roll_review, skype_call: services_skype_call, seminar: services_seminar })
      end
    end
  end
end
