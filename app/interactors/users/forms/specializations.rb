module Users
  module Forms
    class Specializations
      include Users::Form

      attr_accessor :specializations, :kimura, :gi, :armbars, :front_headlock, :smash_punching, :no_gi, :rubber_guard,
                    :peruvian_necktie, :leglocks, :full_guard, :kneebars

      validates :specializations,
                presence: true

      validate :verify_specializations


      def submit(params)
        super
        save_values!(key: object_key, val: specializations)
      end

      private

      def verify_specializations
        errors.add(:specializations, "can't be empty!") if specializations.blank?
      end
    end
  end
end
