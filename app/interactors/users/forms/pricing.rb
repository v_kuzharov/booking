module Users
  module Forms
    class Pricing
      include Users::Form

      attr_accessor :hourly_rate, :number_people, :filming_allowed

      validates :hourly_rate,
                presence: true,
                numericality: true

      validates :number_people,
                presence: true,
                numericality: true

      validates :filming_allowed,
                presence: true


      def submit(params)
        super
        save_values!(key: object_key, val: { hourly_rate: hourly_rate, number_people: number_people, filming_allowed: filming_allowed })
      end
    end
  end
end
