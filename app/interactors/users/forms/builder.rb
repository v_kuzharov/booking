module Users
  module Forms
    class Builder

      def self.for(step, user)
        klass = "Users::Forms::#{step.to_s.camelize}".constantize
        klass.new(user, step)
      end
    end
  end
end
