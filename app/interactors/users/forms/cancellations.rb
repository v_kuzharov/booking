module Users
  module Forms
    class Cancellations
      include Users::Form

      attr_accessor :cancellations, :relaxed, :average, :strict

      validates :cancellations,
                presence: true



      def submit(params)
        super
        save_values!(key: object_key, val: cancellations)
      end
    end
  end
end
