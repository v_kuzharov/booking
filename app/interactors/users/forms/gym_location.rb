module Users
  module Forms
    class GymLocation
      include Users::Form

      attr_accessor :gym_address, :gym_instructions

      validates :gym_address,
                presence: true

      validates :gym_instructions,
                presence: true


      def submit(params)
        super
        save_values!(key: object_key, val: { address: gym_address, instructions: gym_instructions })
      end
    end
  end
end
