module Users
  module Form
    def self.included(base)
      base.class_eval do
        include ActiveModel::Model
      end

      base.send :include, InstanceMethods
    end

    module InstanceMethods
      include ApplicationHelper

      def initialize(user, step)
        @user = user
        @user_data = @user.user_data
        @step = step
        init_instance_variables_for(target_object, prefix: target_prefix, checkbox_list: true)
      end

      def submit(params)
        init_instance_variables_for params

        return unless valid?
      end

      private

      def deconstructing_step
        @ary ||= @step.to_s.split('_')
      end

      def object_key
        @object_key ||= deconstructing_step.first
      end

      def target_object
        @user_data.data[object_key]
      end

      def target_prefix
        return if deconstructing_step.length <= 1

        object_key
      end

      def init_instance_variables_for(object, prefix: nil, checkbox_list: false)
        p = "@#{prefix}"
        p << '_' if prefix.present?

        object&.each do |key, val|
          if key.present? && val.nil?
            instance_variable_set("#{p}#{to_snake_case(key)}", key)
          elsif checkbox_list && val.is_a?(Array)
            val&.each { |item| instance_variable_set("#{p}#{to_snake_case(item)}", item) }
          else
            instance_variable_set("#{p}#{key}", val)
          end
        end
      end

      def save_values!(key:, val:)
        @user_data.data[key] = val
        @user_data.save!
      end
    end
  end
end
