# frozen_string_literal: true

class AvatarUploader < BaseImageUploader

  version :standard do
    process resize_to_fit: [200, 200]
  end

  def default_url(*args)
    '/images/fallback/' + [version_name, 'user-default.png'].compact.join('_')
  end
end
