# frozen_string_literal: true

class MerchantImagesUploader < BaseImageUploader

  version :standard do
    process resize_to_fit: [500, 500]
  end

  version :preview do
    process resize_to_fill: [400, 400]
  end
end
