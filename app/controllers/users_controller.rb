class UsersController < ApplicationController
  skip_before_action :authenticate_user! # remove when html/css will be ready

  def profile
    render_template!('show', user: UserDecorator.decorate(current_user))
  end

  def edit_profile
    render_template!('edit_profile', user: UserDecorator.decorate(current_user))
  end

  def edit_login
    render_template!('edit_login', user: UserDecorator.decorate(current_user))
  end

  def images
    render json: { user_images: current_user.user_images }, status: :ok
  end

  def create_images
    result = Users::CreateImages.call!(images: images_params[:images], user: current_user)

    render json: { user_images: result.user_images }
  rescue InteractorFailure => e
    throw_error!(status: :bad_request, msg: e.custom_errors)
  end

  def delete_images
    image = UserImage.find(params[:id])
    image.destroy!

    render json: { details: :success }, status: :ok
  rescue ActiveRecord::RecordNotFound => e
    render json: { details: e.message }, status: :not_found
  end

  private

  def images_params
    params.require(:user).permit(images: [])
  end
end
