# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  rescue_from ActionPolicy::Unauthorized, with: :user_not_authorized

  protected

  def authenticate_admin!
    authenticate_user!

    unless current_user.admin?
      flash[:alert] = 'Unauthorized Access!'
      redirect_to root_path
    end
  end

  def user_not_authorized(exception)
    action = case exception.policy.to_s
             when 'ProfileBuilderPolicy'
               { path: bookings_profile_index_path, msg: 'You already setup your Profile!' }
             else
               { path: root_path, msg: 'Restricted area!' }
             end

    flash[:alert] = action[:msg]
    redirect_to action[:path]
  end

  def render_template!(template, **attr)
    render template: "#{params[:controller]}/#{params[:action]}/#{template}", locals: attr
  end

  def throw_error!(status: :unprocessable_entity, msg:)
    render json: { details: msg[:errors_msg] }, status: status
  end
end
