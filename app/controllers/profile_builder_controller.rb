class ProfileBuilderController < ApplicationController
  skip_before_action :authenticate_user!, only: :index
  before_action :verify_wizard_step, only: :show

  include Wicked::Wizard

  steps :gym_location, :listing_name, :pricing, :specializations, :images, :services_offered,
        :rules, :cancellations, :payments

  def index; end

  def show
    init_form_and_authorize!

    render_wizard
  end

  def update
    init_form_and_authorize!

    if @wizard_form.submit(params[:user])
      redirect_to wizard_path(next_step)
    else
      render_step(step)
    end
  end

  # After all steps is completed
  def finish_wizard_path
    current_user.update(wizard_complete: true)
    availability_profile_index_path
  end

  private

  def verify_wizard_step
    return redirect_to finish_wizard_path if params[:id].eql?('wicked_finish')

    redirect_to wizard_path(:gym_location) unless wizard_steps.include?(params[:id].to_sym)
  end

  def init_form_and_authorize!
    @wizard_form ||= Users::Forms::Builder.for(step, current_user)

    authorize! @wizard_form, to: :show?, with: ProfileBuilderPolicy
  end
end
