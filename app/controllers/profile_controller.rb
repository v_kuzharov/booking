class ProfileController < ApplicationController
  skip_before_action :authenticate_user! # remove when html/css will be ready
  before_action :find_merchant, only: :show

  def show
    render_template! :show, merchant: MerchantDecorator.decorate(@merchant)
  end

  def manage_availability
    # authorize! current_user, to: :show?, with: ProfilePolicy # Uncomment after adding page html/css styles
    render_template! :manage_availability
  end

  def manage_bookings
    render_template! :manage_bookings
  end

  def messages
    render_template! :messages
  end

  private

  def find_merchant
    @merchant ||= Merchant.with_data.by_id(params[:id])
  end
end
