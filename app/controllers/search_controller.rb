class SearchController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    collection = Merchant.with_data.all # just for stub

    render_template!('index', merchants: MerchantDecorator.decorate_collection(collection))
  end
end
