class AuthController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[login register check_email]


  def login
    result = Users::Login.call!(login_params)
    sign_in(:user, result.user)

    render json: { user: result.user, after_signin_path: result.after_signin_path }
  rescue InteractorFailure => e
    throw_error!(status: :unauthorized, msg: e.custom_errors)
  end

  def register
    result = Users::Register.call!(register_params)
    sign_in(:user, result.user)

    render json: { user: result.user, after_signup_path: result.after_signup_path }
  rescue InteractorFailure => e
    throw_error!(status: :unauthorized, msg: e.custom_errors)
  end

  def check_email
    render json: { is_email_unique: !User.exists?(email: params[:email]) }
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def register_params
    params.require(:user).permit(:email, :password, :first_name, :last_name, :time_zone, :phone, :terms, :entity, :remember)
  end

  def login_params
    params.require(:user).permit(:email, :password)
  end
end
