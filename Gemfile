source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'


gem 'rails', '~> 5.2.3'                                                     # Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'pg', '>= 0.18', '< 2.0'                                                # Use postgresql as the database for Active Record
gem 'puma', '~> 3.11'                                                       # Use Puma as the app server
gem 'sass-rails', '~> 5.0'                                                  # Use SCSS for stylesheets
gem 'uglifier', '>= 1.3.0'                                                  # Use Uglifier as compressor for JavaScript assets
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]          # Windows does not include zoneinfo files, so bundle the tzinfo-data gem

# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

gem 'coffee-rails', '~> 4.2'                                                # Use CoffeeScript for .coffee assets and views
gem 'redis', '~> 4.0'                                                       # Use Redis adapter to run Action Cable in production
gem 'redis-rails'


# Backend
gem 'strong_migrations'                                                     # Catch unsafe migrations at dev time
gem 'draper'                                                                # Draper adds an object-oriented layer of presentation logic to your Rails application.
gem 'sidekiq'                                                               # Simple efficient background processing for Ruby.
gem 'devise'                                                                # Devise is a flexible authentication solution for Rails based on Warden.
gem 'activeadmin'                                                           # Active Admin is a Ruby on Rails framework for creating elegant backends for website administration.
gem 'wicked'                                                                # Rails controllers into step-by-step wizards
gem 'carrierwave', '~> 1.0'                                                 # Simple and extremely flexible way to upload files from Ruby applications
gem 'cloudinary'                                                            # Cloudinary is a cloud service that offers a solution to a web application's entire image management pipeline.
gem 'action_policy', '~> 0.3.0'                                             # Authorization framework for Ruby and Rails applications.

# UI
gem 'hamlit'
gem 'hamlit-rails'                                                          # Haml as the templating engine
gem 'html2haml'
gem 'webpacker', '~> 4.x'                                                   # Webpacker makes it easy to use the JavaScript pre-processor and bundler webpack 4.x.x+ to manage application-like JavaScript in Rails.


group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]                       # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'rmagick'
  gem 'mini_magick'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'spring'                                                              # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'awesome_print', require: 'ap'                                        # Great Ruby dubugging companion: pretty print Ruby objects to visualize their structure.
end

group :test do
  gem 'capybara', '>= 2.15'                                                 # Adds support for Capybara system testing and selenium driver
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'                                                 # Easy installation and use of chromedriver to run system tests with Chrome
end
