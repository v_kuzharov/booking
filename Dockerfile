FROM ruby:2.6.3-stretch

ARG RAILS_MASTER_KEY
ARG RAILS_ENV
ARG RAILS_VERSION
ARG NODE_ENV

RUN apt-get update && apt-get install -y postgresql-client --no-install-recommends
RUN gem install rails --version "$RAILS_VERSION"
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && apt-get install -y gcc g++ make curl software-properties-common nodejs
RUN npm i yarn -g
RUN mkdir -p /src
ADD ./ /bookbjj-src
WORKDIR /bookbjj-src
RUN bundle install
RUN yarn install
RUN echo $RAILS_MASTER_KEY
RUN RAILS_MASTER_KEY=$RAILS_MASTER_KEY RAILS_ENV=$RAILS_ENV bundle exec rails assets:precompile
EXPOSE 3000
CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
