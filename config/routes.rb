# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do

  ActiveAdmin.routes(self)
  mount Sidekiq::Web => '/sidekiq'

  root 'welcome#index'

  devise_for :users,
             controllers: { sessions: 'sessions', registrations: 'registrations' }

  resources :auth, only: [] do
    collection do
      post :register,    action: :register
      post :login,       action: :login
      post :check_email, action: :check_email
    end
  end

  resources :profile_builder, only: %i[index show update], path: 'build_services'

  resources :profile, only: %i[show] do
    collection do
      get :manage_availability, as: :availability
      get :manage_bookings, as: :bookings
      get :messages
    end
  end

  resources :users, only: %i[update] do
    collection do
      get :profile
      get 'edit/login', to: 'users#edit_login'
      get 'edit/profile', to: 'users#edit_profile'
      get :images
      post :create_images
      delete 'delete_images/:id', action: :delete_images
    end
  end

  resources :search, only: %i[index]
end
