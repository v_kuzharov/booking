const { environment } = require('@rails/webpacker');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const vue = require('./loaders/vue');

environment.plugins.prepend('Provide', new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
    jquery: 'jquery',
    Popper: ['popper.js', 'default']
}));

environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin());
environment.loaders.prepend('vue', vue);
module.exports = environment;
