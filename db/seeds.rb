require_relative 'user_seed'
require_relative 'reset_all'

ResetAll.run
UserSeed.seed
