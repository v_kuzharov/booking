# frozen_string_literal: true

require_relative 'seed_base'

class UserSeed < SeedBase
  def self.seed
    super

    # Admins
    admin = Admin.create!(email: 'admin@test.com', password: 'password1', password_confirmation: 'password1', first_name: 'admin', last_name: 'admin')
    admin.create_user_data(data: { phone: '123456789' })

    p '------Admin-created------------'

    # Customers
    customers_data.each do |customer_data|
      c = Customer.create!(customer_data)
      c.create_user_data(data: { phone: '123456789' })
      p "----Customer:--#{c.first_name}-created------------"
    end

    # Merchants
    merchants_data.each do |merchant_data|
      m = Merchant.create!(merchant_data)
      m.create_user_data(data: merchant_additional_data)
      m.user_images.create!(image: File.open(Rails.root.join('app/assets/images/seed/wcw_world_championship.png')))
      p "----Merchant:--#{m.first_name}-created------------"
    end
  end

  def self.reset
    User.destroy_all
  end

  def self.customers_data
    [
        {
            email: 'jeremy@borash.com',
            first_name: 'Jeremy',
            last_name: 'Borash',
            password: '123123',
            password_confirmation: '123123',
            avatar: File.open(Rails.root.join('app/assets/images/seed/jeremyborash.jpg'))
        },
        {
            email: 'eric@bischoff.com',
            first_name: 'Eric',
            last_name: 'Bischoff',
            password: '123123',
            password_confirmation: '123123',
            avatar: File.open(Rails.root.join('app/assets/images/seed/ericbischoff.jpg'))
        }
    ]
  end

  def self.merchants_data
    [
        {
            email: 'sting@sting.com',
            first_name: 'Steve',
            last_name: 'Borden',
            password: '123123',
            password_confirmation: '123123',
            wizard_complete: true,
            avatar: File.open(Rails.root.join('app/assets/images/seed/sting.jpg'))
        },
        {
            email: 'shawn@michaels.com',
            first_name: 'Shawn',
            last_name: 'Michaels',
            password: '123123',
            password_confirmation: '123123',
            wizard_complete: true,
            avatar: File.open(Rails.root.join('app/assets/images/seed/shawnmichaels2.jpg'))
        },
        {
            email: 'rock@therock.com',
            first_name: 'Dwayne',
            last_name: 'Johnson',
            password: '123123',
            password_confirmation: '123123',
            wizard_complete: true,
            avatar: File.open(Rails.root.join('app/assets/images/seed/therock.jpg'))
        },
        {
            email: 'undertaker@theundertaker.com',
            first_name: 'Mark',
            last_name: 'Calaway',
            password: '123123',
            password_confirmation: '123123',
            wizard_complete: true,
            avatar: File.open(Rails.root.join('app/assets/images/seed/theundertaker.jpg'))
        },
        {
            email: 'bret@hart.com',
            first_name: 'Bret',
            last_name: 'Hart',
            password: '123123',
            password_confirmation: '123123',
            wizard_complete: true,
            avatar: File.open(Rails.root.join('app/assets/images/seed/brethart.jpg'))
        }
    ]
  end

  def self.merchant_additional_data
    {
        phone: '123456789',
        gym: {
            address: 'Some street',
            instructions: 'come up to the front door'
        },
        rules: {
            rules_apply: ['No Food', 'No Kids', 'No Recording'],
            rules_travel: '1.5',
            rules_additional: 'please warm up before your session'
        },
        listing: {
            name: 'best leglocker in the world',
            description: "I'll take your leglock game to the next level"
        },
        pricing: {
            hourly_rate: '100',
            number_people: '4',
            filming_allowed: false
        },
        services: {
            seminar: '3000',
            skype_call: '20',
            roll_review: '70',
            private_session: '200'
        },
        cancellations: ['Relaxed', 'Strict'],
        specializations: ['Kimura', 'Armbars', 'Front Headlock', 'Smash Punching', 'Full Guard', 'Kneebars']
    }
  end
end
