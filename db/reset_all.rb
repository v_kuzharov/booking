# frozen_string_literal: true

class ResetAll
  def self.run
    raise 'Are you crazy?' if Rails.env.production?

    UserSeed.reset
  end
end
