class BackfillWizardCompleteColumn < ActiveRecord::Migration[5.2]
  disable_ddl_transaction!

  def change
    User.unscoped.in_batches do |relation|
      relation.update_all wizard_complete: false
      sleep(0.1) # throttle
    end
  end
end
