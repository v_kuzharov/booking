class AddAvatarToUsers < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :avatar, :string
    change_column_default :users, :avatar, ''
  end

  def down
    remove_column :users, :avatar
  end
end
