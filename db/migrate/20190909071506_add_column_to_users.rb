class AddColumnToUsers < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :wizard_complete, :boolean
    change_column_default :users, :wizard_complete, false
  end

  def down
    remove_column :users, :wizard_complete
  end
end
