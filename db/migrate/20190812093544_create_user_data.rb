class CreateUserData < ActiveRecord::Migration[5.2]
  def change
    create_table :user_data do |t|
      t.references :user, foreign_key: true
      t.jsonb :data, default: {}
    end

    add_index :user_data, :data, using: :gin
  end
end
