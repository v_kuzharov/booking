# frozen_string_literal: true

class SeedBase
  def self.seed
    raise 'Are you crazy?' if Rails.env.production?
  end
end
